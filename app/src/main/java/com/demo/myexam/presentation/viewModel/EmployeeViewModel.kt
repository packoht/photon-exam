package com.demo.myexam.presentation.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.demo.myexam.data.EmployeeAction
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class EmployeeViewModel : ViewModel() {

    private val action = MutableLiveData<EmployeeAction>()
    fun getAction(): LiveData<EmployeeAction> = action

    fun getSchoolList() {
        // val response = getJsonFromURL()
        //
        // val myData = Gson().fromJson(response, Array<SchoolResponse>::class.java).toList()
        //
        // val mappedValues = myData.map {
        //     SchoolUiModel(
        //         schoolName = it.schoolName.orEmpty(),
        //         dbn = it.dbn.orEmpty(),
        //         overviewParagraph = it.description.orEmpty()
        //     )
        // }

        // action.value = EmployeeAction.ShowList(mappedValues)
    }

    private fun getJsonFromLocal(): String {

        val url = URL(URL_JSON)
        var reader: BufferedReader? = null
        val stringBuilder = StringBuilder()

        try {
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection

            connection.requestMethod = GET_TYPE
            connection.readTimeout = 15 * 1000
            connection.connect()

            reader = BufferedReader(InputStreamReader(connection.inputStream))

            var line: String?
            while (reader.readLine().also { line = it } != null) {
                stringBuilder.append(line).append("\n")
            }
        } finally {
            reader?.close()
        }

        return stringBuilder.toString()
    }

    private companion object {
        const val URL_JSON = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        const val GET_TYPE = "GET"
    }
}
