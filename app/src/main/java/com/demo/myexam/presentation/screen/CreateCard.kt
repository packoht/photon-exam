package com.demo.myexam.presentation.screen

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.demo.myexam.data.EmployeeModel

@Composable
fun CreateCard(
    modifier: Modifier = Modifier,
    uiModel: EmployeeModel
) {
    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primary
        ),
        modifier = modifier
            .padding(vertical = 12.dp, horizontal = 8.dp)
    ) {
        SchoolContent(uiModel = uiModel)
    }
}