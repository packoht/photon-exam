package com.demo.myexam.presentation.activity

import android.os.Bundle
import android.os.StrictMode
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.demo.myexam.data.EmployeeAction
import com.demo.myexam.data.EmployeeModel
import com.demo.myexam.presentation.screen.CreateCard
import com.demo.myexam.presentation.viewModel.EmployeeViewModel
import com.demo.myexam.ui.theme.MyExamTheme

class EmployeeActivity : ComponentActivity() {

    private val viewModel: EmployeeViewModel by viewModels()

    private var employeeList = mutableListOf<EmployeeModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setContent {
            MyExamTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CreateList(
                        listEmployee = employeeList
                    )
                }
            }
        }
        bindViewModel()
        viewModel.getSchoolList()
    }

    private fun bindViewModel() {
        viewModel.getAction().observe(this, ::handleAction)
    }

    private fun handleAction(action: EmployeeAction) {
        when (action) {
            is EmployeeAction.ShowList -> {
                employeeList = action.listEmployeeModel.toMutableStateList()
            }
        }
    }
}

@Composable
fun CreateList(
    modifier: Modifier = Modifier,
    listEmployee: List<EmployeeModel>
) {
    LazyColumn(modifier = modifier) {
        items(items = listEmployee) {
            CreateCard(uiModel = it)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LoadPreview() {
    CreateCard(
        uiModel = EmployeeModel(
            name = "Jon Doe",
            position = "E1",
            salary = "100000"
        )
    )
}

@Preview
@Composable
fun LoadList() {
    CreateList(
        listEmployee = listOf(
            EmployeeModel(
                name = "Jon Doe",
                position = "E1",
                salary = "100000"
            ),
            EmployeeModel(
                name = "Jon Doe 2",
                position = "E1",
                salary = "10000"
            ),
            EmployeeModel(
                name = "Jon Doe 3",
                position = "E1",
                salary = "1000"
            )
        )
    )
}